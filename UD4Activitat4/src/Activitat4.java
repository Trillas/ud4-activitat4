
public class Activitat4 {

	public static void main(String[] args) {
		//Declaro variables
		int n = 6;
		//Muestro las operaciones
		System.out.println("Valor inicial: " + n);
		System.out.println(n + " + 77 = " + (n+77));
		System.out.println(n + " - 3 = " + (n-3));
		System.out.println(n + " * 2 = " + (n*2));

	}

}
